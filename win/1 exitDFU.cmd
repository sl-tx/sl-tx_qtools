rem @echo off

call "%~dp0params.cmd"
if errorlevel 1 exit /b 1

setlocal enabledelayedexpansion

"%emmcdl_exe%" -p %DFUComPort% -raw 0xFE

echo EDL mode ready
pause
