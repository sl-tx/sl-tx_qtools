@echo off

call "%~dp0params.cmd"
if errorlevel 1 exit /b 1


if not exist "%FirehoseFile%" (

  echo Firehose file "%FirehoseFile%" not found
  pause
  exit /b 1

)

if not exist "%RawProgFile%" (

  echo Raw program file "%RawProgFile%" not found
  pause
  exit /b 1

)


setlocal enabledelayedexpansion



"%emmcdl_exe%" -p %EDLComPort% -f "%FirehoseFile%" -x "%BackupProgFile%"

if errorlevel 1 (

  pause
  exit /b 1

)

echo All partitions downloaded to the device
pause
