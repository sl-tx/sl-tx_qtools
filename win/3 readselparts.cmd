@echo off

call "%~dp0params.cmd"
if errorlevel 1 exit /b 1



if not exist "%SelPartsFile%" (
  echo Selected partitions file "%SelPartsFile%" not found
  pause
  exit /b 1
)

if not exist "%FirehoseFile%" (
  echo Firehose file "%FirehoseFile%" not found
  pause
  exit /b 1
)



setlocal enabledelayedexpansion



set NumParts=0

for /f "usebackq" %%P in ("%SelPartsFile%") do (

  "%emmcdl_exe%" -p %EDLComPort% -f "%FirehoseFile%" -d %%P -o "%~dp0/backup/%%P.bin"

  if errorlevel 1 (
    pause
    exit /b 1
  )

  set /a NumParts = !NumParts! + 1
)

echo %NumParts% partition images created
pause
