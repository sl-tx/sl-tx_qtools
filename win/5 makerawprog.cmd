@echo off

rem Call ourself to convert from dec to hex from "for /f"

if "%~1" == "-hex" (

  call :DecToHex "%~2"
  exit /b %errorlevel%

)





call "%~dp0params.cmd"
if errorlevel 1 exit /b 1



if not exist "%PartTableFile%" (

  echo Partition table file "%PartTableFile%" not found.
  echo Please make it first
  pause
  exit /b 1

)

if not exist "%TargetPartsFile%" (

  echo Selected partitions file "%TargetPartsFile%" not found
  pause
  exit /b 1

)



echo ^<?xml version="1.0" ?^>>"%RawProgFile%"
echo ^<data^>>>"%RawProgFile%"



setlocal enabledelayedexpansion



for /f "usebackq tokens=1-4" %%A in ("%PartTableFile%") do (

  if not "%%A" == "#" (

    for /f "usebackq" %%P in ("%TargetPartsFile%") do (

      if "%%P" == "%%B" (

        rem Calculate byte offset in 256-byte units to avoid overflow

        set /a ByteOffset256=%%C * 2

        set /a SizeInKB=%%D / 2

        for /f %%Z in ('call "%~f0" -hex !ByteOffset256!') do set ByteOffset256Hex=%%Z

        echo   ^<program SECTOR_SIZE_IN_BYTES="512" file_sector_offset="0" filename="target\%%B.bin" label="%%B" num_partition_sectors="%%D" physical_partition_number="0" size_in_KB="!SizeInKB!.0" sparse="false" start_byte_hex="0x!ByteOffset256Hex!00L" start_sector="%%C"/^>>>"%RawProgFile%"

      )

    )

  )

)

echo ^</data^>>>"%RawProgFile%"

echo Raw program script saved to "%RawProgFile%"
pause

exit /b 0





:DecToHex

setlocal enabledelayedexpansion

set DecNum=%~1
set DigTable=0123456789abcdef
set HexRes=

:Loop

set /a DecNumQ=%DecNum% / 16
set /a DecRmd=%DecNum% - %DecNumQ% * 16
set DecNum=%DecNumQ%

set HexDig=!DigTable:~%DecRmd%,1!

set HexRes=%HexDig%%HexRes%

if %DecNum% neq 0 goto Loop

echo %HexRes%

endlocal

exit /b 0
