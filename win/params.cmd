rem @echo off

set EDLComPort=com4
set DFUComPort=com6
set FirehoseFile=%~dp0\firehose\prog_emmc_firehose_SDM450.mbn

set SelPartsFile=%~dp0selparts.txt
set TargetPartsFile=%~dp0targetparts.txt
set PartTableFile=%~dp0parttable.txt
set BackupProgFile=%~dp0rawprogram0.xml
set RawProgFile=%~dp0rawprogram1.xml
set emmcdl_exe=%~dp0\emmcdl\emmcdl_2_15.exe
